<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;

class InstrumentsController extends CrudResourceController
{
  public function pages()
  {
    $page = $this->request->getQuery('page', 'int', 0);
    $limit = $this->request->getQuery('limit', 'int', 10);

    if ($page <= 0) {
      throw new \Exception('Page is necessary');
    }

    $data = \App\Model\Instruments::find();
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $response = [
      'items' =>  $result->items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }
}
