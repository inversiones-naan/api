<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;
use App\Constants\Services;
use Phalcon\Di;

class FundingController extends CrudResourceController
{
  public function pages()
  {
    $page = $this->request->getQuery('page', 'int', 0);
    $limit = $this->request->getQuery('limit', 'int', 10);
    $wordProject = $this->request->getQuery('wordProject', 'string', '');
    $wordUser = $this->request->getQuery('wordUser', 'string', '');
    $userId = $this->request->getQuery('userId', 'int', 0);
    $projectId = $this->request->getQuery('projectId', 'int', 0);
    
    if ($page <= 0) {
      throw new \Exception('Page is necessary');
    }
    $columns = \App\Model\Funding::columns();
    array_push($columns, 'users.username');
    array_push($columns, 'users.firstName');
    array_push($columns, 'users.lastName');
    array_push($columns, 'projects.name');
    array_push($columns, 'users.status as userStatus');
    array_push($columns, 'projects.status as projectStatus');
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns($columns)
    ->from(['funding'=> 'App\Model\Funding'])
    ->leftJoin('App\Model\Users', 'users.id = funding.userId', 'users')
    ->leftJoin('App\Model\Projects', 'projects.id = funding.projectId', 'projects')
    ->where('funding.id > 0');

    if ($userId != 0) {
      $objQuery->andWhere('funding.userId = :userId:', ['userId'=> $userId]);
    }

    if ($projectId != 0) {
      $objQuery->andWhere('funding.projectId = :projectId:', ['projectId'=> $projectId]);
    }

    if ($wordUser != '') {
      $objQuery->andWhere('users.username LIKE :wordUser: OR users.firstName LIKE :wordUser: OR users.lastName LIKE :wordUser:', ['word'=>"%".$wordUser."%"]);
    }

    if ($wordProject != '') {
      $objQuery->andWhere('projects.name LIKE :wordProject:', ['word'=>"%".$wordProject."%"]);
    }

    $data = $objQuery
    ->getQuery()
    ->execute();
    
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $response = [
      'items' =>  $result->items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }
}
