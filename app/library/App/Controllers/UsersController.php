<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\InclusionIn;
use Phalcon\Validation\Validator\Date as DateValidator;
use App\Constants\AclRoles;

use App\Constants\Services;

class UsersController extends CrudResourceController
{
  public function me ()
  {
    return $this->createResourceResponse($this->userService->getDetails());
  }

  public function meedit ()
  {
    $userAction = null;
    if ($this->authManager->loggedIn()) {
      $session = $this->authManager->getSession();
      $userId = $session->getIdentity(); // For example; 1
      $userAction = \App\Model\Users::findFirstById($userId);
    }
    if ($userAction == null) {
      throw new \Exception('No tienes acceso');
    }
    $putData = $this->request->getJsonRawBody(true);
    foreach ($userAction as $key => $value) {
      if (in_array($key, array("id", "status", "code", "createdAt", "updatedAt", "role", "username", "password"))) {
        continue;
      }
      if (!isset($putData[$key])) {
        return;
      }
      $userAction->$key = $putData[$key];
    }
    $userAction->update();
    $transformer = new \App\Transformers\UsersTransformer;
    $transformer->setModelClass('App\Model\Users');

    $user = $this->createItemResponse(\App\Model\Users::findFirst($session->getIdentity()), $transformer);

    return $this->createArrayResponse($user, 'user');
  }

  public function authenticate()
  {
    $username = $this->request->getUsername();
    $password = $this->request->getPassword();

    $session = $this->authManager->loginWithUsernamePassword(\App\Auth\UsernameAccountType::NAME, $username, $password);

    $transformer = new \App\Transformers\UsersTransformer;
    $transformer->setModelClass('App\Model\Users');

    $user = $this->createItemResponse(\App\Model\Users::findFirst($session->getIdentity()), $transformer);

    $response = [
      'token' => $session->getToken(),
      'expires' => $session->getExpirationTime(),
      'user' => $user
    ];

    return $this->createArrayResponse($response, 'data');
  }

  public function whitelist()
  {
    return [
      'firstName',
      'lastName',
      'password'
    ];
  }

  public function pages()
  {
    $page = $this->request->getQuery('page', 'int', 1);
    $limit = $this->request->getQuery('limit', 'int', 10);

    if ($page <= 0) {
      throw new \Exception('Page is necessary');
    }

    $data = \App\Model\Users::find();
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $response = [
      'items' =>  $result->items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }
  public function changeUser() // No fue posible llamar este metodo update
  {
    $postedData = $this->request->getJsonRawBody(true);
    if (!isset($postedData['id'])) {
      throw new \Exception('Id is necessary');
    }
    $user = \App\Model\Users::findFirst($postedData['id']);
    if (!$user) {
      throw new \Exception('User not fount');
    }
    $user->update();
    return $this->createArrayResponse($user->toArray(), 'payload');
  }

  public function sigin ()
  {
    // Validar la data
    $data = $this->request->getJsonRawBody(true);
    $validation = new Validation();
    // // Nombres
    // $validation->add(
    //   'firstName',
    //   new StringLength([
    //     'messageMaximum' => 'El nombre debe tener maximo 255 caracteres',
    //     'max'            => 255,
    //     'messageMinimum' => 'El nombre debe minimo 3 caracteres',
    //     'min'            => 3,
    //   ])
    // );
    // // Apellidos
    // $validation->add(
    //   'lastName',
    //   new StringLength([
    //     'messageMaximum' => 'Los apellidos deben tener maximo 255 caracteres',
    //     'max'            => 255,
    //     'messageMinimum' => 'Los apellidos deben tener minimo 3 caracteres',
    //     'min'            => 3,
    //   ])
    // );
    // // Fecha de necimiento
    // $validation->add(
    //   'birthdate',
    //   new DateValidator([
    //     "format"  => "Y-m-d",
    //     "message" => "La fecha de nacimiento es invalida",
    //   ])
    // );
    // // Nacionalidad
    // $validation->add(
    //   'nationality',
    //   new InclusionIn([
    //     "message" => "La nacionalidad debe ser 0 o 1",
    //     "domain"  => [0, 1],
    //   ])
    // );
    // // Inversionista calificado
    // $validation->add(
    //   'qualifiedInvestor',
    //   new InclusionIn([
    //     "message" => "Inversionista calificado debe ser 0 o 1",
    //     "domain"  => [0, 1],
    //   ])
    // );
    // // rfc
    // $validation->add(
    //   'rfc',
    //   new StringLength([
    //     'messageMaximum' => 'EL RFC debe tener maximo 255 caracteres',
    //     'max'            => 255,
    //     'messageMinimum' => 'El RFC debe tener minimo 3 caracteres',
    //     'min'            => 3,
    //   ])
    // );
    // Correo electronico
    $validation->add(
      'email',
      new Email([
        'message' => 'El correo debe ser valido',
      ])
    );
    $validation->add(
      'email',
      new StringLength([
        'messageMaximum' => 'El correo debe tener maximo 255 caracteres',
        'max'            => 255,
        'messageMinimum' => 'El correo debe tener minimo 0 caracteres',
        'min'            => 0,
      ])
    );
    // Contraseña
    $validation->add(
      'password',
      new StringLength([
        'messageMaximum' => 'La contraseña debe tener maximo 255 caracteres',
        'max'            => 255,
        'messageMinimum' => 'La contraseña debe tener minimo 8 caracteres',
        'min'            => 8,
      ])
    );
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error con los datos del registro de usuario');
    }

    // Validar que el correo no exite
    $toldUser = \App\Model\Users::findFirst('username = "'.$data['email'].'"');
    if ($toldUser) {
      throw new \Exception('El correo ya ha sido usado');
    }
    // Crear el codigo de validación
    $totalUsers = \App\Model\Users::count();
    $numRam = 254 - strlen($totalUsers)-4;
    $data['code'] = 'code'.$totalUsers.$this->randChar($numRam);

    $user = new \App\Model\Users();
    /** @var \Phalcon\Security $security */
    $security = $this->di->get(Services::SECURITY);
    $data['username'] = $data['email'];
    unset($data['email']);
    $data['password'] = $security->hash($data['password']);
    $user->role = AclRoles::USER;
    $user->status = 0; // Estado de creado pero no validado
    if ($user->create($data) == false) {
      foreach ($entity->getMessages() as $message) {
        if (method_exists($message, 'getMessage')) {
          throw new \Exception($message->getMessage());
        }
      }
      throw new \Exception('Error al crear cuenta');
    }

    // Enviar el correo
    $mail = $this->di->get(Services::MAIL_SERVICE);
    /** @var \Phalcon\Config $config */
    $config = $this->di->get(Services::CONFIG);
    try {
      $mail->send(
        'Registro de usuario',
        $data['username'],
        'Completar registro',
        'confirmationAccount',
        array(
          'confirmUrl'=> $config->clientHostName.'/activate-account?code='.$data['code']
        )
      );
    }  catch (\Exception $e) {
      throw new \Exception($e->getMessage(), 500);
    }

    return $this->createOkResponse();
    // return $this->createArrayResponse(array($data), 'data');
  }
  private function randChar($length)
  {
    $options = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $code = "";
    for($i = 0; $i < $length; $i++)
    {
      $key = rand(0, strlen($options) - 1);
      $code .= $options[$key];
    }    
    return $code;
  }
  /**
   * Activar la cuenta de usuario
   *
   * @return void
   */
  public function activateaccount ()
  {
    // Validar la data
    $data = $this->request->getJsonRawBody(true);

    if (!isset($data['code'])) {
      throw new \Exception('No existe el código de activación');
    }
    if (substr($data['code'], 0, 3) == 'code') {
      throw new \Exception('No corresponde a un codigo de activación');      
    }
    $session = $this->authManager->loginWithCode(\App\Auth\CodeAccountType::NAME, $data['code']);
    
    $transformer = new \App\Transformers\UsersTransformer;
    $transformer->setModelClass('App\Model\Users');
    
    $user = \App\Model\Users::findFirst($session->getIdentity());
    $user->status = 1;
    $user->code = NULL;
    $user->update();

    $user = $this->createItemResponse($user, $transformer);

    $response = [
      'token' => $session->getToken(),
      'expires' => $session->getExpirationTime(),
      'user' => $user
    ];

    return $this->createArrayResponse($response, 'data');
  }
  /**
   * Solicitar nueva contraseña
   *
   * @return void
   */
  public function newpassword ()
  {
    $data = $this->request->getJsonRawBody(true);
    if (!isset($data['email'])) {
      throw new \Exception('No existe el correo electrónico');
    }
    $validation = new Validation();
    $validation->add(
      'email',
      new Email([
        'message' => 'El correo debe ser valido',
      ])
    );
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error con los datos al renovar la contraseña');
    }
    $user = \App\Model\Users::findFirst('username = "'.$data['email'].'"');
    if (!$user) {
      throw new \Exception('Error con los datos al renovar la contraseña');
    }
    if ($user->status != 1) {
      throw new \Exception('No puedes recuperar la contraseña, su cuenta no esta activa');
    }

    $totalUsers = \App\Model\Users::count();
    $numRam = 254 - strlen($totalUsers)-8;
    $data['code'] = 'password'.$totalUsers.$this->randChar($numRam);
    
    // Enviar el correo
    $mail = $this->di->get(Services::MAIL_SERVICE);
    /** @var \Phalcon\Config $config */
    $config = $this->di->get(Services::CONFIG);
    try {
      $mail->send(
        'Recuperar contraseña',
        $user->username,
        'Proceso para recuperar contraseña',
        'newpasswordAccount',
        array(
          'confirmUrl'=> $config->clientHostName.'/activate-password?code='.$data['code']
        )
      );
    }  catch (\Exception $e) {
      throw new \Exception($e->getMessage(), 500);
    }
    $user->code = $data['code'];
    $user->update();
    return $this->createOkResponse();
  }

  public function activatepassword ()
  {
    // Validar la data
    $data = $this->request->getJsonRawBody(true);
     
    if (!isset($data['code'])) {
      throw new \Exception('No existe el código de activación');
    }
    if (substr($data['code'], 0, 7) == 'password') {
      throw new \Exception('No corresponde a un codigo de recuperación de la contraseña');      
    }
    $session = $this->authManager->loginWithCode(\App\Auth\CodeAccountType::NAME, $data['code']);
    
    $transformer = new \App\Transformers\UsersTransformer;
    $transformer->setModelClass('App\Model\Users');
    
    $user = \App\Model\Users::findFirst($session->getIdentity());
    if ($user->status != 1) {
      throw new \Exception('El usuario no esta activo');
    }
    $user->code = NULL;
    $user->update();

    $user = $this->createItemResponse($user, $transformer);

    $response = [
      'token' => $session->getToken(),
      'expires' => $session->getExpirationTime(),
      'user' => $user
    ];

    return $this->createArrayResponse($response, 'data');
  }

  public function updateaccount ($id)
  {
    $user = \App\Model\Users::findFirst($id);
    if (!$user) {
      throw new \Exception('El usuario no existe');
    }
    // Validar la data
    $data = $this->request->getJsonRawBody(true);
    
    $userAction = null;
    if ($this->authManager->loggedIn()) {
      $session = $this->authManager->getSession();
      $userId = $session->getIdentity(); // For example; 1
      $userAction = \App\Model\Users::findFirstById($userId);
    }
    if ($userAction == null) {
      throw new \Exception('No tienes acceso');
    }
    // Quitar valores que va a ser modificados
    $deleteChanges = ['status', 'id', 'createdAt', 'updatedAt'];
    foreach ($deleteChanges as $key => $value) {
      if (isset($data[$key])) {
        unset($data[$key]);
      }
    }

    // ¿El usuario no es administrador?
    if ($userAction->status != AclRoles::ADMINISTRATOR) {
      $deleteChanges = ['role']; // No podrá modificar el role si no es el administrador
      foreach ($deleteChanges as $key => $value) {
        if (isset($data[$key])) {
          unset($data[$key]);
        }
      }
    }

    if (isset($data['password'])) { // Validar contraseña
      if (strlen($data['password'])>1) {
        if (strlen($data['password'])<8) {
          throw new \Exception('La contraseña minimo debe tener 8 characteres');
        }      
        /** @var \Phalcon\Security $security */
        $security = $this->di->get(Services::SECURITY);
        $data['password'] = $security->hash($data['password']);
      } else {
        unset($data['password']);
      }
    }

    $validation = new Validation();
    $validation->add(
      'username',
      new Email([
        'message' => 'El correo debe ser valido',
      ])
    );

    $userEmail = \App\Model\Users::findFirstByUsername($data['username']);
    if ($userEmail) {
      if ($userEmail->id != $id) {
        throw new \Exception('EL correo ya existe');
      }
    }

    $user->update($data);
    return $this->createOkResponse();
  }

  public function newactivate ()
  {
    $data = $this->request->getJsonRawBody(true);
    if (!isset($data['email'])) {
      throw new \Exception('No existe el correo electrónico');
    }
    $validation = new Validation();
    $validation->add(
      'email',
      new Email([
        'message' => 'El correo debe ser valido',
      ])
    );
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error con los datos al solicitar aactivar la cuenta');
    }
    // Validar que el correo no exite
    $user = \App\Model\Users::findFirst('username = "'.$data['email'].'"');
    if (!$user) {
      throw new \Exception('La cuenta no existe');
    }
    // Crear el codigo de validación
    $totalUsers = \App\Model\Users::count();
    $numRam = 254 - strlen($totalUsers)-4;
    $user->code = 'code'.$totalUsers.$this->randChar($numRam);

    // Enviar el correo
    $mail = $this->di->get(Services::MAIL_SERVICE);
    /** @var \Phalcon\Config $config */
    $config = $this->di->get(Services::CONFIG);
    try {
      $mail->send(
        'Activar cuenta',
        $user->username,
        'Instrucciones para completar registro',
        'confirmationAccount',
        array(
          'confirmUrl'=> $config->clientHostName.'/activate-account?code='.$user->code
        )
      );
    }  catch (\Exception $e) {
      throw new \Exception($e->getMessage(), 500);
    }
    $user->update();
    return $this->createOkResponse();
  }
  public function create ()
  {
    $data = $this->request->getJsonRawBody(true);
    $validation = new Validation();
    $validation->add(
      'email',
      new Email([
        'message' => 'El correo debe ser valido',
      ])
    );
    $validation->add(
      'password',
      new StringLength([
        'messageMaximum' => 'La contraseña debe tener maximo 255 caracteres',
        'max'            => 255,
        'messageMinimum' => 'La contraseña debe tener minimo 8 caracteres',
        'min'            => 8,
      ])
    );
    $validation->add(
      "role",
      new InclusionIn([
        "message" => "El role deberia ser ".implode(' or ', AclRoles::ALL_ROLES),
        "domain"  => AclRoles::ALL_ROLES,
      ])
    );
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error con los datos al crear el usuario');
    }
    $data['username'] = $data['email'];
    $user = \App\Model\Users::findFirstByUsername($data['username']);
    if ($user) {
      throw new \Exception('EL correo ya existe');
    }

    $user = new \App\Model\Users();
    /** @var \Phalcon\Security $security */
    $security = $this->di->get(Services::SECURITY);
    $data['username'] = $data['email'];
    unset($data['email']);
    $data['password'] = $security->hash($data['password']);
    $data['status'] = 1;
    if (isset($data['sendEmail'])) {
      if ($data['sendEmail'] == '1') {
        $data['status'] = 0;
        // Crear el codigo de validación
        $totalUsers = \App\Model\Users::count();
        $numRam = 254 - strlen($totalUsers)-4;
        $data['code'] = 'code'.$totalUsers.$this->randChar($numRam);
      }
    }
    if ($user->create($data) == false) {
      foreach ($entity->getMessages() as $message) {
        if (method_exists($message, 'getMessage')) {
          throw new \Exception($message->getMessage());
        }
      }
      throw new \Exception('Error al crear usuario');
    }
    if ($data['status'] == 0 ) {
      // Enviar el correo
      $mail = $this->di->get(Services::MAIL_SERVICE);
      /** @var \Phalcon\Config $config */
      $config = $this->di->get(Services::CONFIG);
      try {
        $mail->send(
          'Registro de usuario',
          $data['username'],
          'Completar registro',
          'confirmationAccount',
          array(
            'confirmUrl'=> $config->clientHostName.'/activate-account?code='.$data['code']
          )
        );
      }  catch (\Exception $e) {
        throw new \Exception($e->getMessage(), 500);
      }
    }
    return $this->createArrayResponse($user->toArray(), 'user');
  }
}
