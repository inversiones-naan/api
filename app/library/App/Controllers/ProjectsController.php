<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;
use App\Constants\Services;
use Phalcon\Di;

// Constants
use App\Constants\FundingStatus;
use App\Constants\FundingTypes;
use App\Constants\ProjectStatus;
use App\Constants\UserProjectStatus;

class ProjectsController extends CrudResourceController
{
  public function pages()
  {
    $page = $this->request->getQuery('page', 'int', 0);
    $limit = $this->request->getQuery('limit', 'int', 10);
    $word = $this->request->getQuery('word', 'string', '');
    
    if ($page <= 0) {
      throw new \Exception('Page is necessary');
    }
    $columns = \App\Model\Projects::columns();
    array_push($columns,'count( usersProjects.id ) as totalUsers');
    // array_push($columns,'count( funding.id ) as totalTransactions');
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns($columns)
    ->from(['projects'=> 'App\Model\Projects'])
    ->leftJoin('App\Model\UsersProjects', 'projects.id = usersProjects.projectId', 'usersProjects');
    // ->leftJoin('App\Model\Funding', 'projects.id = funding.projectId', 'funding');
    if ($word != '') {
      $objQuery->where('projects.name LIKE :word:', ['word'=>"%".$word."%"]);
    }
    $data = $objQuery
    ->orderBy('name')
    ->groupBy('projects.id')
    ->getQuery()
    ->execute();
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $items = [];
    foreach ($result->items as $item) {
      $totalTransactions = \App\Model\Funding::count([
        "projectId = ?0",
        "bind" => [
          $item->id
        ]
      ]);
      $totalImages = \App\Model\ProjectsImages::count([
        "projectId = ?0",
        "bind" => [
          $item->id
        ]
      ]);
      $item = $item->toArray();
      $item['totalTransactions'] = $totalTransactions;
      $item['totalImages'] = $totalImages;
      array_push($items, $item);
    }
    $response = [
      'items' =>  $items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }
  
  public function userspages () {
    $projectId = $this->request->getQuery('id', 'int', 0);
    $page = $this->request->getQuery('page', 'int', 0);
    $limit = $this->request->getQuery('limit', 'int', 10);
    $word = $this->request->getQuery('word', 'string', '');

    if ($projectId <= 0) {
      throw new \Exception('Id is necessary');
    }
    if ($page <= 0) {
      throw new \Exception('Page is necessary');
    }

    $columns = \App\Model\Users::columns();

    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns($columns)
    ->from(['users'=> 'App\Model\Users'])
    ->leftJoin('App\Model\UsersProjects', 'users.id = usersProjects.userId', 'usersProjects')  
    ->where('usersProjects.projectId = :id:', ['id'=> $projectId]);
    if ($word != '') {
      $objQuery->andWhere('users.username LIKE :word: OR users.firstName LIKE :word: OR users.lastName LIKE :word:', ['word'=>"%".$word."%"]);
    }

    $data = $objQuery
    ->orderBy('users.username, users.status')
    ->groupBy('users.id, usersProjects.userId')
    ->getQuery()
    ->execute();
    
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $response = [
      'items' =>  $result->items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }

  public function suggestions () {
    $projectsObj = \App\Model\Projects::find('status > 0')->toArray();
    shuffle($projectsObj);
    $projectsObj = array_splice($projectsObj, 0, 4);
    $projects = array();
    foreach ($projectsObj as $object) {
      $project = \App\Model\Projects::findFirst($object['id']);
      $images = $project->getImages(['id', 'title', 'url']);
      $project = $project->toArray();
      $project['images'] = $images;
      array_push($projects, $project);
    }
    return $this->createArrayResponse($projects, 'projects');
  }

  public function portafolio () {
    // Obtener el identificador del usuario
    $session = $this->authManager->getSession();
    $userId = $session->getIdentity();

    // Total invertido
    $totalInvestment = \App\Model\Funding::sum([
      "column"     => "value",
      "conditions" => "userId = :userId: AND status = :status: AND type = :type:",
      "bind"       => ['userId' => $userId, 'status' => FundingStatus::ACTIVE, 'type' => FundingTypes::INVEST]
    ]);
    $totalInvestment = $totalInvestment ? $totalInvestment : 0;

    // Inversión pendiente : Inversion realizada en proyectos que estan en fondeo
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns(['users.id', 'SUM(funding.value) as totalInvestment'])
    ->from(['users'=> 'App\Model\Users'])
    ->leftJoin('App\Model\Funding', 'users.id = funding.userId', 'funding')
    ->leftJoin('App\Model\Projects', 'projects.id = funding.projectId', 'projects')
    ->where('users.id = :userId:', ['userId'=> $userId])
    ->andWhere('funding.status = :statusF: AND funding.type = :type: AND projects.status = :statusP:', ['statusF' => FundingStatus::ACTIVE, 'type' => FundingTypes::INVEST, 'statusP' => ProjectStatus::INVESTING])
    ->getQuery()
    ->getSingleResult();
    $pendingInvestment = $objQuery->totalInvestment;
    $pendingInvestment = $pendingInvestment ? $pendingInvestment : 0;
    $pendingInvestmentPercent = $totalInvestment ? number_format((float) ($pendingInvestment / $totalInvestment * 100), 2, '.', ',') : 0;

    // Inversión en construcción : Inversión realizada en proyectos que estan en construcción.
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns(['users.id', 'SUM(funding.value) as investmentInConstruction'])
    ->from(['users'=> 'App\Model\Users'])
    ->leftJoin('App\Model\Funding', 'users.id = funding.userId', 'funding')
    ->leftJoin('App\Model\Projects', 'projects.id = funding.projectId', 'projects')
    ->where('users.id = :userId:', ['userId'=> $userId])
    ->andWhere('funding.status = :statusF: AND funding.type = :type: AND projects.status = :statusP:', ['statusF' => FundingStatus::ACTIVE, 'type' => FundingTypes::INVEST, 'statusP' => ProjectStatus::INVESTING_COMPLETED])
    ->getQuery()
    ->getSingleResult();
    $investmentInConstruction = $objQuery->investmentInConstruction;
    $investmentInConstruction = $investmentInConstruction ? $investmentInConstruction : 0;
    $investmentInConstructionPercent = $totalInvestment ? number_format((float) ($investmentInConstruction / $totalInvestment * 100), 2, '.', ',') : 0;

    // Dinero al inversionista : Dinero que se le ha retornado - ~/total invertido
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns(['users.id', 'SUM(funding.value) as moneyInvestor'])
    ->from(['users'=> 'App\Model\Users'])
    ->leftJoin('App\Model\Funding', 'users.id = funding.userId', 'funding')
    ->leftJoin('App\Model\Projects', 'projects.id = funding.projectId', 'projects')
    ->where('users.id = :userId:', ['userId'=> $userId])
    ->andWhere('funding.status = :statusF: AND funding.type = :type:', ['statusF' => FundingStatus::ACTIVE, 'type' => FundingTypes::PAY])
    ->getQuery()
    ->getSingleResult();
    $moneyInvestor = $objQuery->moneyInvestor;
    $moneyInvestor = $moneyInvestor ? $moneyInvestor : 0;
    $moneyInvestorPercent = $totalInvestment ? number_format((float) ($moneyInvestor / $totalInvestment * 100), 2, '.', ',') : 0;

    // Rendimiento al dia :
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns(['users.id', 'SUM(funding.value * projects.pmtPercent) as actualPerformance'])
    ->from(['users'=> 'App\Model\Users'])
    ->leftJoin('App\Model\Funding', 'users.id = funding.userId', 'funding')
    ->leftJoin('App\Model\Projects', 'projects.id = funding.projectId', 'projects')
    ->where('users.id = :userId:', ['userId'=> $userId])
    ->andWhere('funding.status = :statusF: AND funding.type = :type:', ['statusF' => FundingStatus::ACTIVE, 'type' => FundingTypes::PAY])
    ->getQuery()
    ->getSingleResult();
    $actualPerformance = $objQuery->actualPerformance;
    $actualPerformance = $actualPerformance ? $actualPerformance : 0;
    $actualPerformancePercent = $moneyInvestor > 0 ? number_format((float) ($actualPerformance / $moneyInvestor * 100), 2, '.', ',') : 0;

    // Encontrar los proyectos en los cuales participa el usuario
    $objQuery = Di::getDefault()->get(Services::MODELS_MANAGER)->createBuilder()
    ->columns(['projects.id', 'projects.name'])
    ->from(['projects'=> 'App\Model\Projects'])
    ->leftJoin('App\Model\UsersProjects', 'projects.id = usersProjects.projectId', 'usersProjects')
    ->where('usersProjects.userId = :userId: AND usersProjects.status = :status:', ['userId'=> $userId, 'status' => UserProjectStatus::ACTIVE])
    ->getQuery()
    ->execute();
    $projects = $objQuery->toArray();

    $portafolio = array(
      'pendingInvestment' => array(
        'value' => number_format((float) $pendingInvestment, 2, '.', ','),
        'percent' => $pendingInvestmentPercent
      ),
      'investmentInConstruction' => array(
        'value' => number_format((float) $investmentInConstruction, 2, '.', ','),
        'percent' => $investmentInConstructionPercent
      ),
      'actualPerformance' => array(
        'value' => number_format((float) $actualPerformance, 2, '.', ','),
        'percent' => $actualPerformancePercent
      ),
      'moneyInvestor' => array(
        'value' => number_format((float) $moneyInvestor, 2, '.', ','),
        'percent' => $moneyInvestorPercent
      ),
      'projects' => $projects
    );
    return $this->createArrayResponse($portafolio, 'portafolio');
  }
}
