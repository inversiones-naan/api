<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;

use App\Constants\Services;

class BuildersController extends CrudResourceController
{
  public function pages()
  {
    $page = $this->request->getQuery('page', 'int', 0);
    $limit = $this->request->getQuery('limit', 'int', 10);

    if ($page <= 0) {
      throw new \Exception('Es necesario la pagina');
    }

    $data = \App\Model\Builders::find();
    $paginator = new PaginatorModel(
      array(
        "data" => $data,
        "limit"=> $limit,
        "page" => $page
      )
    );
    $result = $paginator->getPaginate();
    $response = [
      'items' =>  $result->items,
      'before'=>  $result->before,
      'next'  =>  $result->next,
      'last'  =>  $result->last,
      'current' =>  $result->current,
      'total_pages'=>  $result->total_pages
    ];
    return $this->createArrayResponse($response, 'payload');
  }
  public function create()
  {
    $response = [];
    $di = new \PhalconRest\Di\FactoryDefault;
    $request = $di->get(Services::REQUEST);
    $name = $request->getPost('name', 'string', NULL);
    if ($name == NULL) {
      throw new \Exception('Es necesario el nombre de la constructora');
    }
    $description = $request->getPost('description');
    $url = $request->getPost('url');
    $path = NULL;
    // Check if the user has uploaded files
    if ($this->request->hasFiles()) {
      $files = $this->request->getUploadedFiles();
      // Print the real file names and sizes
      $random = new \App\Services\Random();
      foreach ($files as $file) {
        // define a “unique” name and a path to where our file must go
        $path = 'uploaded/b-'.$random->token(50).'-'.$file->getname();
        // Move the file into the application
        $file->moveTo($path);
      }
    }
    // if ($path == NULL) {
    //   throw new \Exception('Es necesario agregar una imagen');
    // }
    $builder = new \App\Model\Builders();
    $builder->name = $name;
    $builder->description = $description;
    $builder->url = $url;
    $builder->urlImage = $path;
    $builder->create();

    return $this->createArrayResponse($builder->toArray(), 'builder');
  }

  public function update($id)
  {
    $builder = \App\Model\Builders::findFirst($id);
    if (!$builder) {
      throw new \Exception('La constructora no existe');
    }
    $response = [];
    $di = new \PhalconRest\Di\FactoryDefault;
    $request = $di->get(Services::REQUEST);
    $name = $request->getPost('name', 'string', NULL);
    if ($name == NULL OR $name == '') {
      throw new \Exception('Es necesario el nombre de la constructora');
    }
    $description = $request->getPost('description', 'string', NULL);
    $url = $request->getPost('url');
    $path = NULL;
    // Check if the user has uploaded files
    if ($this->request->hasFiles()) {
      $files = $this->request->getUploadedFiles();
      // Print the real file names and sizes
      $random = new \App\Services\Random();
      foreach ($files as $file) {
        // define a “unique” name and a path to where our file must go
        $path = 'uploaded/b-'.$random->token(50).'-'.$file->getname();
        // Move the file into the application
        $file->moveTo($path);
      }
    }
    if ($path != NULL) {
      if (file_exists($builder->urlImage)) {
        unlink($builder->urlImage);
      }
      $builder->urlImage = $path;
    }
    $builder->name = $name;
    $builder->description = $description;
    $builder->url = $url;
    $builder->update();

    return $this->createArrayResponse($builder->toArray(), 'builder');
  }
}
