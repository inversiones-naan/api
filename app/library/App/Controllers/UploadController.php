<?php

namespace App\Controllers;

use PhalconRest\Mvc\Controllers\CrudResourceController;
use \Phalcon\Paginator\Adapter\Model as PaginatorModel;

use App\Constants\Services;

class UploadController extends CrudResourceController
{
  public function projectimages()
  {
    $response = [];
    $di = new \PhalconRest\Di\FactoryDefault;
    $request = $di->get(Services::REQUEST);
    $projectId = $request->getPost('projectId', 'int', 0);
    // if ($projectId == 0) {
    //   throw new \Exception('ProjectId is neccesary');
    // }
    array_push($response, $projectId);
    // Check if the user has uploaded files
    if ($this->request->hasFiles()) {
      $files = $this->request->getUploadedFiles();
      
      // Print the real file names and sizes
      foreach ($files as $file) {
        // Print file details
        // echo $file->getName(), ' ', $file->getSize(), '\n';
        array_push($response, $file->getName(). ' '. $file->getSize());
        // define a “unique” name and a path to where our file must go
        $path = 'uploaded/'.$this->randChar(60).'-'.$file->getname();
        // Move the file into the application
        $file->moveTo($path);

        $projectsImages = new \App\Model\ProjectsImages();
        $projectsImages->projectId = $projectId;
        $projectsImages->title = $file->getname();
        $projectsImages->url = $path;
        $projectsImages->create();
      }
    } else {
      throw new \Exception('Images not found');
    }
    return $this->createArrayResponse($response, 'payload');
  }
  private function randChar($length)
  {
    $options = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $code = "";
    for($i = 0; $i < $length; $i++)
    {
      $key = rand(0, strlen($options) - 1);
      $code .= $options[$key];
    }    
    return $code;
  }
}
