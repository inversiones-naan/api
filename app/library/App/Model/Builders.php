<?php

namespace App\Model;

class Builders extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $name;

  public function getSource()
  {
    return 'builders';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'name' => 'name',
      'description' => 'description',
      'url' => 'url',
      'url_image' => 'urlImage'
    ];
  }

  public function initialize() {
    $this->hasMany('id', Projects::class, 'builderId', [
      'alias' => 'Projects',
    ]);
    // $this->belongsTo('albumId', Album::class, 'id', [
    //   'alias' => 'Album',
    // ]);
  }
}
