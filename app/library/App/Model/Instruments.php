<?php

namespace App\Model;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\StringLength;

class Instruments extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $name;

  public function getSource()
  {
    return 'instruments';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'name' => 'name'
    ];
  }

  public function initialize()
  {
    $this->hasMany('id', Projects::class, 'instrumentId', [
      'alias' => 'Projects',
    ]);
  }

  public function beforeCreate()
  {
    $this->validateData();
  }
  public function beforeUpdate()
  {
    $this->validateData();
  }
  private function validateData () {
    $validation = new Validation();
    $validation->add(
      'name',
      new StringLength([
        'messageMinimum' => ':field minimun 8 characters',
        'min'            => 6,
        'messageMaximum' => ':field maximum 255 characters',
        'max'            => 255,
      ])
    );
    $messages = $validation->validate($this->toArray());
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error instrument');
    }
  }

}
