<?php

namespace App\Model;

class ProjectsNews extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $projectId;
  public $creatorId;
  public $text;

  public function getSource()
  {
    return 'projects_news';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'project_id' => 'projectId',
      'creator_id' => 'creatorId',
      'text' => 'text'
    ];
  }

  public function initialize() {
    $this->belongsTo('projectId', Projects::class, 'id', [
      'alias' => 'Project',
    ]);

    $this->belongsTo('creatorId', Users::class, 'id', [
      'alias' => 'Creator',
    ]);
  }
}
