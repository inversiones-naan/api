<?php

namespace App\Model;

class UsersProjects extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $projectId;
  public $userId;
  public $status;

  public function getSource()
  {
    return 'users_projects';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'project_id' => 'projectId',
      'user_id' => 'userId',
      'status' => 'status' // 0 pendiente, 1 Activo, 2 Eliminado
    ];
  }

  public function initialize()
  {
    $this->belongsTo('projectId', Projects::class, 'id', [
      'alias' => 'Projects',
    ]);
    $this->belongsTo('userId', Users::class, 'id', [
      'alias' => 'Users',
    ]);
  }

  public static function columns ()
  {
    return [
      'usersProjects.id',
      'usersProjects.projectId',
      'usersProjects.userId',
      'usersProjects.status',
      'usersProjects.createdAt',
      'usersProjects.updatedAt'
    ];
  }
}
