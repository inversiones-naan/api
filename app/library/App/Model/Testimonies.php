<?php

namespace App\Model;

class Testimonies extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $name;
  public $description;
  public $url;

  public function getSource()
  {
    return 'testimonies';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'name' => 'name',
      'description' => 'description',
      'url' => 'url'
    ];
  }

  public function initialize() {

    // $this->belongsTo('albumId', Album::class, 'id', [
    //   'alias' => 'Album',
    // ]);
  }
}
