<?php

namespace App\Model;

class ProjectsImages extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $projectId;
  public $title;
  public $url;
  public $status;

  public function getSource()
  {
    return 'projects_images';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'project_id' => 'projectId',
      'title' => 'title',
      'url' => 'url',
      'status' => 'status'
    ];
  }

  public function initialize()
  {
    $this->belongsTo('projectId', Projects::class, 'id', [
      'alias' => 'Projects',
    ]);
  }

  public function beforeDelete ()
  {
    // Eliminar el archivo
    $filepath = ROOT_DIR.'/public/'.$this->url;
    if (is_file($filepath)) {
      unlink($filepath);
    }
  }
}
