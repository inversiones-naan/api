<?php

namespace App\Model;

// Servicios
use Phalcon\Di;
use App\Constants\Services;
use App\Constants\AclRoles;
use App\Constants\FundingStatus;
use App\Constants\FundingTypes;
use App\Constants\ProjectStatus;
use App\Constants\UserProjectStatus;

// Validadores
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\InclusionIn;
use \Phalcon\Validation\Validator\Regex as RegexValidator;

// Modelos
use \App\Model\Projects;
use \App\Model\Users;

class Funding extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $projectId;
  public $userId;
  public $creatorId;
  // public $fund;
  public $value;
  public $type;
  public $status;

  public function getSource()
  {
    return 'funding';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'project_id' => 'projectId', // Proyecto a donde se fondea
      'user_id' => 'userId', // Usario a quien se le asigna el monto (valor)
      'creator_id' => 'creatorId', // Usario quien realizó la transacción
      // 'fund' => 'fund', // Total transacción
      'value' => 'value', // Valor de transaccion
      'type' => 'type', // 0 si es deposito del inversionista, 1 es pago al inversionista por rendex
      'status' => 'status' // 0 pendiente, 1 confirmada, 2 cancelado
    ];
  }

  public function initialize()
  {
    $this->belongsTo('userId', Users::class, 'id', [
      'alias' => 'Users',
    ]);
    $this->belongsTo('creatorId', Users::class, 'id', [
      'alias' => 'Creators',
    ]);
    $this->belongsTo('projectId', Projects::class, 'id', [
      'alias' => 'Projects',
    ]);
  }

  public static function columns ()
  {
    return [
      'funding.id',
      'funding.projectId',
      'funding.userId',
      'funding.creatorId',
      'funding.value',
      'funding.type',
      'funding.status',
      'funding.createdAt',
      'funding.updatedAt'
    ];
  }

  public function beforeCreate()
  {
    $this->validateData();

    // Formato de moneda para Mexico
    setlocale(LC_MONETARY,"es_MX"); // en_US

    $this->status = FundingStatus::PENDING; // Por defecto, el fondeo no estará activado, solo se activará por un administrador desde el admin
    $project = $this->getProjects();
    if ($project->status == ProjectStatus::INACTIVE) {
      throw new \Exception('El proyecto esta inactivo, no es posible fondear');
    }
    if ($this->type == 0) {
      if ($this->value < $project->minimumInvestment) {
        throw new \Exception('El monto mínimo a invertir en este proyecto es '.money_format("%i", $project->minimumInvestment));
      }
    }    

    $di = new \PhalconRest\Di\FactoryDefault;
    
    // Obtener el creador
    $authManager = Di::getDefault()->get(Services::AUTH_MANAGER); // EN un modelo solo de esta forma funciona el administrador de autorización
    if ($authManager->loggedIn()) {
      $session = $authManager->getSession();
      $this->creatorId = $session->getIdentity();
      $userAction = \App\Model\Users::findFirstById($this->creatorId);
      $role = $userAction->role;
    } else {
      throw new \Exception('Es necesario que el usuario este identificado');
    }
    if ($this->type == FundingTypes::PAY) { // Solo podrá pagar al inversionista un administrador
      if (!in_array($role, AclRoles::ADMIN_ROLES)) {
        throw new \Exception('No puedes registrar pago a inversionista');
      }
    }
    // Enviar correo a la persona indicada
    $request = $di->get(Services::REQUEST);
    $postedData = $request->getJsonRawBody(true);    
    if (isset($postedData['emailing'])) { // Si la peticion contiene esta propiedad indica que se debe enviar por correo
      try {
        // Enviar el correo
        $mail = $this->di->get(Services::MAIL_SERVICE);
        /** @var \Phalcon\Config $config */
        $config = $this->di->get(Services::CONFIG);

        $project = Projects::findFirst($this->projectId);
        $user = Users::findFirst($this->userId);

        if ($this->type == FundingTypes::INVEST) { // EL correo se enviará si la transacción es interés de invertir
          foreach ($config->emailFounding as $email) {
            $mail->send(
              'Fondear Proyecto',
              $email,
              'Estan interesados en fondear el proyecto',
              'foundingProjectInfo',
              array(
                'user'=> $user,
                'project'=> $project,
                'value'=> number_format((float) $this->value, 2, '.', ',')
              )
            );
          }
        }        
      } catch (\Exception $e) {
        throw new \Exception($e->getMessage(), 500);
      }
    }
    // Secreará la relación entre el usuario y el proyecto de su interes
    $userProject = UsersProjects::findFirst('projectId = '.$this->projectId.' AND userId = '.$this->userId);
    if (!$userProject) {
      $userProject = new UsersProjects();
      $userProject->userId = $this->userId;
      $userProject->projectId = $this->projectId;
      $userProject->status = UserProjectStatus::ACTIVE; // Hasta que al menos no se confirme un solo fondo activo el usuario no queda activo al proyecto, no queda activo como usuario del 
      $userProject->create();
    }
  }

  public function afterCrete()
  {
    // Se recalcula el avance del fondo del proyecto
    $this->calculateProgress();
  }

  public function beforeValidationOnUpdate ()
  {
    $this->validateData();
  }
  private function validateData ()
  {
    // Validar los datos
    $validation = new Validation();
    $validation->add(
      "projectId",
      new RegexValidator([
        "pattern" => "/^[1-9]\d*$/",
        "message" => "No es un identificador válido del proyecto",
      ])
    );
    $validation->add(
      "userId",
      new RegexValidator([
        "pattern" => "/^[1-9]\d*$/",
        "message" => "No es un identificador válido para el usuario",
      ])
    );
    $validation->add(
      "value",
      new RegexValidator([
        "pattern" => "/^[1-9]\d*$/",
        "message" => "EL monto debe ser un entero y mayor que cero",
      ])
    );
    $validation->add(
      "type",
      new InclusionIn([
        "message" => 'No es un tipo valido de transacción ['.implode(FundingTypes::ALL).']',
        "domain"  => FundingTypes::ALL,
      ])
    );
    $data = $this->toArray();
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error al crear la transacción');
    }
  }
  /**
   * Solo los administradores podran modificar la transacción
   *
   * @return void
   */
  public function afterUpdate () {
    $this->calculateProgress();
    $userProject = UsersProjects::findFirst('projectId = '.$this->projectId.' AND userId = '.$this->userId);
    if (!$userProject) {
      $userProject = new UsersProjects();
      $userProject->userId = $this->userId;
      $userProject->projectId = $this->projectId;
      $userProject->status = UserProjectStatus::ACTIVE; // Hasta que al menos no se confirme un solo fondo activo el usuario no queda activo al proyecto, no queda activo como usuario del 
      $userProject->create();
    }

    if ($userProject->status == UserProjectStatus::DELECTED) { // Usuario eliminado
      return;
    }
    // Total invertido en el proyecto por el usuario
    $sum = Funding::sum([
      "column"     => "value",
      "conditions" => "projectId = :projectId: AND userId = :userId: AND status = :status: AND type = :type:",
      "bind"       => [
        'userId' => $this->userId,
        'projectId' => $this->projectId,
        'status' => FundingStatus::ACTIVE,
        'type' => FundingTypes::INVEST
      ]
    ]);
    
    // Según los montos, puede cambiar el estado de un proyecto
    $project = $userProject->getProjects();
    if ($sum >= $project->funding) {
      if ($project->status = ProjectStatus::INVESTING) { // Al alcanzar el fondeo, y al estar en estado en fondeo pasa al estado fondeado
        $project->status = ProjectStatus::INVESTING_COMPLETED;
        $project->update();
      }
    } else {
      if ($project->status == ProjectStatus::INVESTING_COMPLETED) { // Si el proyecto aún no ha alcanzado el fondo y esta en fondeo completo, pasa al estado en fondeo
        $project->status = ProjectStatus::INVESTING;
        $project->update();
      }
    }
  }
  /**
   * Calcula el porcentaje de fondeo del proyecto
   *
   * @return void
   */
  private function calculateProgress () {
    $total = Funding::sum([
      "column"     => "value",
      "conditions" => "projectId = :projectId: AND status = :status: AND type = :type:",
      "bind"       => [
        'projectId' => $this->projectId,
        'status' => FundingStatus::ACTIVE,
        'type' => FundingTypes::INVEST
      ]
    ]);
    $project = $this->getProjects();
    if (!is_numeric($project->funding)){
      $project->funding = 0;
    }
    if (!is_numeric($total)){
      $total = 0;
    }
    
    if ($project->funding == 0) {
      $project->progressFunding = '0.00';
    } else {
      $project->progressFunding = number_format((float)($total / $project->funding * 100), 2, '.', '');
    }
    $project->update();
  }
}
