<?php

namespace App\Model;

use App\Constants\Services;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\InclusionIn;
use \Phalcon\Validation\Validator\Digit;

// Services
use \App\Services\Finances;

class Projects extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $name;
  public $albumId;
  public $address;
  public $fixedAnnualRate;
  public $instrumentId;
  public $term;
  public $funding;
  public $progressFunding;
  public $possibilityOfPrepaid;
  public $minimumInvestment;
  public $status;
  public $description;
  public $investment;
  public $googleLat;
  public $googleLng;
  public $pmt;
  public $pmtPercent;

  public function getSource()
  {
    return 'projects';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'name' => 'name', // Nombre del proyecto
      'builder_id' => 'builderId', // Identificador de la constructora
      'address' => 'address', // Dirección del proyecto
      'fixed_annual_rate' => 'fixedAnnualRate', // Tasa fija anual
      'instrument_id' => 'instrumentId', // Identificador del instrumento
      'term' => 'term', // Plazo en meses
      'funding' => 'funding', // Fondo a complir
      'progress_funding' => 'progressFunding', // Progreso en el fondeo
      'possibility_of_prepaid' => 'possibilityOfPrepaid', // Posibilidad de pago
      'minimum_investment' => 'minimumInvestment', // Inversión minima por persona
      'status' => 'status', // Estado del proyecto, 0 inactivo, 1 iniciar fondeo, 2 fondeo completo, 3 generando retornos, 4 proyecto terminado
      'description'=> 'description',
      'investment'=> 'investment',
      'googleLat'=> 'googleLat',
      'googleLng'=> 'googleLng',
      'pmt'=> 'pmt',
      'pmt_percent' => 'pmtPercent'
    ];
  }

  public function initialize()
  {
    $this->belongsTo('builderId', Builders::class, 'id', [
      'alias' => 'Builder',
    ]);
    $this->belongsTo('instrumentId', Instruments::class, 'id', [
      'alias' => 'Instrument',
    ]);
    $this->hasMany('id', ProjectsImages::class, 'projectId', [
      'alias' => 'Images',
    ]);
    $this->hasMany('id', UsersProjects::class, 'projectId', [
      'alias' => 'Users',
    ]);
    $this->hasMany('id', Funding::class, 'projectId', [
      'alias' => 'Funding',
    ]);
    $this->hasMany('id', ProjectsNews::class, 'projectId', [
      'alias' => 'News',
    ]);
    
    // Valores por defecto
    if (!$this->id) {
      $this->funding = 0;
      $this->progressFunding = '00.00';
    }
    $this->pmt = NULL;
    $this->pmtPercent = NULL;
  }
  /**
   * Lista de parametros por defecto
   *
   * @return Array
   */
  public static function columns ()
  {
    return [
      'projects.id',
      'projects.name',
      'projects.address',
      'projects.fixedAnnualRate',
      'projects.term',
      'projects.funding',
      'projects.progressFunding',
      'projects.possibilityOfPrepaid',
      'projects.minimumInvestment',
      'projects.status',
      'projects.pmt',
      'projects.pmtPercent',
      'projects.status',
      'projects.createdAt',
      'projects.updatedAt'
    ];
  }
  public function beforeCreate () {
    $di = new \PhalconRest\Di\FactoryDefault;
    $request = $di->get(Services::REQUEST);
    $data = $request->getJsonRawBody(true);
    
    $this->validateData($data);
    $this->pmt = Finances::pmt($this->fixedAnnualRate, $this->term, $this->funding);    
    $this->pmtPercent = Finances::pmtPercent($this->pmt, $this->term, $this->funding);
  }
  // Sobreescribir el evento de actualizar y ver como cambia los estados de las tareas
  public function beforeUpdate()
  {
    $di = new \PhalconRest\Di\FactoryDefault;
    $request = $di->get(Services::REQUEST);

    $data = $this->toArray();

    $this->validateData($data);
    
    $this->updatedAt = date('Y-m-d H:i:s');
    if ($this->status == 1) { // Si se actualiza verificar que si estan totalente fondeado se pase a ese estado
      if ((float) $this->progressFunding > 100) {
        $this->status == 2;
      }
    }
    $this->pmt = Finances::pmt($this->fixedAnnualRate, $this->term, $this->funding);
    $this->pmtPercent = Finances::pmtPercent($this->pmt, $this->term, $this->funding);
  }

  private function validateData ($data)
  {
    $validation = new Validation();
    
    // $validation->add(
    //   [        
    //     'name',
    //     'address',
    //     'fixedAnnualRate',
    //     // 'term',
    //     'progressFunding',
    //     'possibilityOfPrepaid',
    //     'minimumInvestment'
    //   ],
    //   new StringLength(
    //     [
    //       'messageMinimum' => [
    //         'name' => ':field minimun 3 characters',
    //         'address' => ':field minimun 3 characters',
    //         'fixedAnnualRate' => ':field minimun 3 characters',
    //         // 'term' => ':field minimun 3 characters',
    //         'progressFunding' => ':field minimun 3 characters',
    //         'possibilityOfPrepaid' => ':field minimun 3 characters',
    //         'minimumInvestment' => ':field minimun 3 characters'
    //       ],
    //       'min'            => [
    //         'name' => 3,
    //         'address' => 3,
    //         'fixedAnnualRate' => 3,
    //         'term' => 3,
    //         'progressFunding' => 3,
    //         'possibilityOfPrepaid' => 3,
    //         'minimumInvestment' => 3
    //       ],
    //       'messageMaximum' => [
    //         'name' => ':field maximum 255 characters',
    //         'address' => ':field maximum 255 characters',
    //         'fixedAnnualRate' => ':field maximum 255 characters',
    //         'term' => ':field maximum 255 characters',
    //         'progressFunding' => ':field maximum 255 characters',
    //         'possibilityOfPrepaid' => ':field maximum 255 characters',
    //         'minimumInvestment' => ':field maximum 255 characters'
    //       ],
    //       'max'            =>  [
    //         'name' => 255,
    //         'address' => 255,
    //         'fixedAnnualRate' => 255,
    //         'term' => 255,
    //         'progressFunding' => 255,
    //         'possibilityOfPrepaid' => 255,
    //         'minimumInvestment' => 255
    //       ]
    //     ]
    //   )
    // );
    // $validation->add(
    //   [
    //     'builderId',
    //     'instrumentId',
    //     'funding',
    //   ],
    //   new Digit([
    //     'message' => [
    //       'builderId'=> ':field must be numeric',
    //       'instrumentId'=> ':field must be numeric',
    //       'funding'=> ':field must be numeric'
    //     ]
    //   ])
    // );
    // $messages = $validation->validate($data);
    // if (count($messages) > 0) {
    //   foreach ($messages as $message) {
    //     throw new \Exception($message);
    //   }
    //   throw new \Exception('Error project');
    // }
  }
}
