<?php

namespace App\Model;
use Phalcon\Di;
use App\Constants\Services;
use App\Constants\AclStatus;
use App\Constants\AclRoles;
use \Phalcon\Validation;
use \Phalcon\Validation\Validator\Email;
use \Phalcon\Validation\Validator\StringLength;
use \Phalcon\Validation\Validator\InclusionIn;

class Users extends \App\Mvc\DateTrackingModel
{
  public $id;
  public $role;
  // public $email;
  public $username;
  public $password;
  public $firstName;
  public $lastName;
  public $rfc;
  public $street;
  public $ext;
  public $int;
  public $colony;
  public $city;
  public $cp;
  public $state;
  public $country;
  public $interbankClabe;
  public $birthdate;
  public $status;
  public $code;
  
  public function getSource()
  {
    return 'users';
  }

  public function columnMap()
  {
    return parent::columnMap() + [
      'id' => 'id',
      'role' => 'role',
      // 'email' => 'email',
      'username' => 'username',
      'password' => 'password',
      'first_name' => 'firstName',
      'last_name' => 'lastName',
      'rfc' => 'rfc',
      'street' => 'street',
      'ext' => 'ext',
      'int' => 'int',
      'colony' => 'colony',
      'city' => 'city',
      'cp' => 'cp',
      'state' => 'state',
      'country' => 'country',
      'interbank_clabe' => 'interbankClabe',
      'birthdate' => 'birthdate',
      'status' => 'status', // 0 pendiente, 1 activado, 2 baneado, 3 inactivo
      'code' => 'code'
    ];
  }

  public function initialize()
  {
    $this->hasMany('id', UsersProjects::class, 'userId', [
      'alias' => 'UsersProjects',
    ]);
    $this->hasMany('id', Funding::class, 'userId', [
      'alias' => 'Funding',
    ]);
  }

  // public function beforeCreate()
  // {
  //   $this->getJsonRawBody('something'); // Obtener los datos del request

  //   if ($this->password == '' OR $this->password == NULL) {
  //     $this->password = $this->getJsonRawBody('password');
  //     if ($this->password == NULL) {
  //       throw new \Exception('Password empty '.$this->password);
  //     }
  //   }
  //   /** @var \Phalcon\Security $security */
  //   $security = Di::getDefault()->get(Services::SECURITY);
  //   $this->password = $security->hash($this->password);

  //   if ($this->role == '' OR $this->role == NULL) {
  //     $this->role = $this->getJsonRawBody('role');
  //     if ($this->role == NULL) {
  //       throw new \Exception('Role empty');
  //     }
  //   }

  //   if ($this->username == '' OR $this->username == NULL) {
  //     $this->username = $this->getJsonRawBody('username');
  //     if ($this->username == NULL) {
  //       throw new \Exception('Username empty');
  //     }
  //   }

  //   $user = \App\Model\Users::findFirstByUsername($this->username);

  //   if ($user) {
  //     throw new \Exception('Username already exist');
  //   }

  //   if ($this->email == '' OR $this->email == NULL) {
  //     $this->email = $this->getJsonRawBody('email');
  //     if ($this->email == NULL) {
  //       throw new \Exception('Email empty');
  //     }
  //   }

  //   $user = \App\Model\Users::findFirstByEmail($this->email);
    
  //   if ($user) {
  //     throw new \Exception('Email already exist');
  //   }

  //   $this->valitateDataUser($this->postedData);

  //   $sendEmail = $this->getJsonRawBody('sendEmail');
  //   if ($sendEmail) {
  //     $this->status = AclStatus::PENDING;
  //   } else {
  //     $this->status = AclStatus::ACTIVE;
  //   }
  // }

  private function getJsonRawBody ($prop = NULL)
  {
    if ($prop == NULL) {
      return NULL;
    }
    if (!$this->postedData) {
      $request = Di::getDefault()->get('request');
      $this->postedData = $request->getJsonRawBody(true);
    }
    return isset($this->postedData[$prop]) ? $this->postedData[$prop] : NULL;
  }

  // public function afterCreate ()
  // {
  //   $sendEmail = $this->getJsonRawBody('sendEmail');
  //   if ($sendEmail) {
  //     // Enviar correo
  //   }
  // }

  public function beforeValidationOnUpdate()
  {
    // En PUT request se agregan los siguientes campos
    // firstName
    // lastName
    
    // Los siguientes campos vienen en request pero no se agregan en automatico en el modelo
    // email
    // passwordRenew
    // role
    // biography

    // $this->getJsonRawBody('something'); // Obtener los datos del request

    // $this->valitateDataUser($this->postedData);

    // // Los siguiente campos se actualizan en el request PUT
    // if (isset($this->postedData['id'])) {
    //   unset($this->postedData['id']);
    // }
    // if (isset($this->postedData['passwordRenew'])) {
    //   /** @var \Phalcon\Security $security */
    //   $security = Di::getDefault()->get(Services::SECURITY);
    //   $this->password = $security->hash($this->postedData['passwordRenew']);
    //   unset($this->postedData['passwordRenew']);
    // }
    // foreach ($this->postedData as $key => $value) {
    //   $this->$key = $value;
    // }
  }
  /**
   * Validar los datos del usuario
   *
   * @param Array $data
   * @return void
   */
  private function valitateDataUser($data) {
    if (!is_array($data)) {
      return;
    }
    $validation = new Validation();
    // Validar si es un correo
    if (isset($data['email'])) {
      $validation->add(
        'email',
        new Email([
          'message' => 'The e-mail is not valid',
        ])
      );
      $validation->add(
        'email',
        new StringLength([
          'messageMaximum' => 'email maximum 255 characters',
          'max'            => 255,
          'messageMinimum' => 'email minimun 0 characters',
          'min'            => 0,
        ])
      );
    }
    // Validar Contraseña
    if (isset($data['password'])) {
      $validation->add(
        'password',
        new StringLength([
          'messageMinimum' => 'password minimun 8 characters',
          'min'            => 8,
          'messageMaximum' => 'password maximum 255 characters',
          'max'            => 255,
        ])
      );
    }
    // Validar nueva contraseña
    if (isset($data['passwordRenew'])) {
      $validation->add(
        'passwordRenew',
        new StringLength([
          'messageMinimum' => 'password minimun 8 characters',
          'min'            => 8,
          'messageMaximum' => 'password maximum 255 characters',
          'max'            => 255,
        ])
      );
    }
    // Validar role
    if (isset($data['role'])) {
      $validation->add(
        "role",
        new InclusionIn([
          "message" => "role must be ".implode(' or ', AclRoles::ALL_ROLES),
          "domain"  => AclRoles::ALL_ROLES,
        ])
      );
    }
    $messages = $validation->validate($data);
    if (count($messages) > 0) {
      foreach ($messages as $message) {
        throw new \Exception($message);
      }
      throw new \Exception('Error user');
    }
  }
  public static function columns () {
    return [
      'users.id',
      'users.role',
      'users.username',
      'users.password',
      'users.firstName',
      'users.lastName',
      'users.rfc',
      'users.street',
      'users.ext',
      'users.int',
      'users.colony',
      'users.city',
      'users.cp',
      'users.state',
      'users.city',
      'users.interbankClabe',
      'users.birthdate',
      'users.status',
      'users.code'
    ];
  }
}
