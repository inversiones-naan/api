<?php

namespace App\Services;

use App\Constants\Services;
use Phalcon\Mvc\View;

use Swift;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_Mailer;

class Mail extends \PhalconApi\User\Service
{

  protected $_transport;

  /**
   * @desc  Sends e-mails via gmail based on predefined templates
   *
   * @param array $to
   * @param string $subject
   * @param string $nameTemplate
   * @param array $paramsTemplate
   */
  public function send($fromName, $to, $subject, $nameTemplate, $paramsTemplate)
  {
    /** @var \Phalcon\Config $config */
    $config = $this->di->get(Services::CONFIG);

    $mailSettings = $config->mail;
    $template = $this->getTemplate($nameTemplate, $paramsTemplate);

    // Create the message
    $message = Swift_Message::newInstance()
      ->setSubject($subject)
      ->setTo($to)
      ->setFrom(array(
        $mailSettings->fromEmail => $fromName
      ))
      ->setBody($template, 'text/html');
    if (!$this->_transport) {
      $this->_transport = Swift_SmtpTransport::newInstance(
        $mailSettings->smtp->server,
        $mailSettings->smtp->port,
        $mailSettings->smtp->security
      )
      ->setUsername($mailSettings->smtp->username)
      ->setPassword($mailSettings->smtp->password);
    }

    // Create the Mailer using your created Transport
    $mailer = Swift_Mailer::newInstance($this->_transport);

    return $mailer->send($message);
  }

  /**
   * Applies a template to be used in the e-mail
   *
   * @param string $nameTemplate
   * @param array $paramsTemplate
   */
  public function getTemplate ($nameTemplate, $paramsTemplate)
  {
    /** @var \Phalcon\Config $config */
    $config = $this->di->get(Services::CONFIG);

    $parameters = array_merge(array(
      'publicUrl' => $config->clientHostName
    ), $paramsTemplate);

    $viewService = $this->di->get(Services::VIEW);

    return $viewService->getRender(
      'emailTemplates',
      $nameTemplate,
      $parameters,
      function ($view) {
        $view->setRenderLevel(View::LEVEL_LAYOUT);
    });
  }
  public function getName () {
    return 'Servicio de correos';
  }
}