<?php

namespace App\Services;

class Random
{
  /**
   * Crea una cadena aleatoria
   *
   * @param Integer $length
   * @return String
   */
  public function token($length)
  {
    $options = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $code = "";
    for($i = 0; $i < $length; $i++)
    {
      $key = rand(0, strlen($options) - 1);
      $code .= $options[$key];
    }    
    return $code;
  }
  public function getName () {
    return 'Servicio de aleatorios';
  }
}