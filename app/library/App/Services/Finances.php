<?php

namespace App\Services;

class Finances 
{
  /**
   * Obtiene el pmt
   *
   * @param [String] $interest Intereses
   * @param [Integer] $months Meses
   * @param [type] $loan Monto del préstamo
   * @return void
   */
  public static function pmt($interest, $months, $loan)
  {
    $interest = $interest / 1200;
    $amount = $interest * - $loan * pow((1 + $interest), $months) / (1 - pow((1 + $interest), $months));
    return $amount;
    return 0;
  }
  public static function pmtPercent ($pmt, $months, $loan)
  {
    $pmtNeto = $loan / $months;
    return $pmt/$pmtNeto - 1;
  }
  public function getName () {
    return 'Servicio de Finanzas';
  }
}