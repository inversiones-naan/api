<?php

namespace App\Auth;

use App\Constants\Services;
use Phalcon\Di;

class CodeAccountType implements \PhalconApi\Auth\AccountType
{
  const NAME = "code";

  public function login($data)
  {
    $code = $data[Manager::LOGIN_DATA_CODE];

    /** @var \App\Model\Users $user */
    $user = \App\Model\Users::findFirst([
      'conditions' => 'code = :code:',
      'bind' => ['code' => $code]
    ]);

    if (!$user) {
      return null;
    }

    return (string)$user->id;
  }

  public function authenticate($identity)
  {
    return \App\Model\Users::count([
      'conditions' => 'id = :id:',
      'bind' => ['id' => (int)$identity]
    ]) > 0;
  }
}
