<?php

namespace App\Auth;

use PhalconApi\Auth\Session;
use PhalconRest\Exception;

class Manager extends \PhalconApi\Auth\Manager
{
  const LOGIN_DATA_EMAIL = 'email';
  const LOGIN_DATA_CODE = 'code';

  /**
   * @param string $accountTypeName
   * @param string $email
   * @param string $password
   *
   * @return Session Created session
   * @throws Exception
   *
   * Helper to login with email & password
   */
  public function loginWithEmailPassword($accountTypeName, $email, $password)
  {
    return $this->login($accountTypeName, [

      self::LOGIN_DATA_EMAIL => $email,
      self::LOGIN_DATA_PASSWORD => $password
    ]);
  }

  public function loginWithCode($accountTypeName, $code)
  {
    return $this->login($accountTypeName, [      
      self::LOGIN_DATA_CODE => $code
    ]);
  }
}