<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\ProjectsNews;
use App\Transformers\ProjectsNewsTransformer;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class ProjectsNewsResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('ProjectsNews')
      ->model(ProjectsNews::class)
      ->expectsJsonData()
      ->transformer(ProjectsNewsTransformer::class)
      ->handler(CrudResourceController::class)
      ->itemKey('projectNew')
      ->collectionKey('projectsNews')
      ->deny(AclRoles::UNAUTHORIZED)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::create()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED)
      )
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED))
      ->endpoint(ApiEndpoint::remove()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED)
      );
  }
}
