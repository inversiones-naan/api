<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\UsersProjects;
use App\Transformers\UsersProjectsTransformer;
use App\Constants\AclRoles;
use App\Controllers\UsersprojectsController;
// use PhalconRest\Mvc\Controllers\CrudResourceController;

class UsersProjectsResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('UsersProjects')
      ->model(UsersProjects::class)
      ->expectsJsonData()
      ->transformer(UsersProjectsTransformer::class)
      ->itemKey('userProject')
      ->collectionKey('UsersProjects')
      ->deny(AclRoles::UNAUTHORIZED)
      ->handler(UsersprojectsController::class)

      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns transactions using pagination: ?page={int}&limit={int}&userId={int}&projectId={int}&wordUser={string}&wordProject={string}')
        ->name("pages")
      );
  }
}
