<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\Projects;
use App\Transformers\ProjectsTransformer;
use App\Constants\AclRoles;
use App\Controllers\ProjectsController;

class ProjectsResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Projects')
      ->model(Projects::class)
      ->expectsJsonData()
      ->transformer(ProjectsTransformer::class)
      ->handler(ProjectsController::class)
      ->itemKey('project')
      ->collectionKey('projects')
      ->deny(AclRoles::UNAUTHORIZED)

      ->endpoint(ApiEndpoint::all()
        ->allow(AclRoles::UNAUTHORIZED)
      )
      ->endpoint(ApiEndpoint::create())
      ->endpoint(ApiEndpoint::find()
        ->allow(AclRoles::UNAUTHORIZED)
      )
      ->endpoint(ApiEndpoint::update())
      ->endpoint(ApiEndpoint::remove())
      ->endpoint(ApiEndpoint::get('/suggestions', 'suggestions')
        ->allow(AclRoles::UNAUTHORIZED)
        ->name("projects-suggestions")
      )
      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns projects using pagination: ?page={int}&limit={int}')
        ->name("projects")
      )
      ->endpoint(ApiEndpoint::get('/portafolio', 'portafolio')
        ->allow(AclRoles::USER)
        ->description('Returns my portafolio')
        ->name("portafolio")
      );
  }
}
