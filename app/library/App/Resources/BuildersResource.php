<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\Builders;
use App\Transformers\BuildersTransformer;
use App\Constants\AclRoles;
use App\Controllers\BuildersController;

class BuildersResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Builders')
      ->model(Builders::class)
      ->expectsJsonData()
      ->transformer(BuildersTransformer::class)
      ->itemKey('builder')
      ->collectionKey('builders')
      ->deny(AclRoles::UNAUTHORIZED)
      ->handler(BuildersController::class)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update())
      ->endpoint(ApiEndpoint::remove())
      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns builders using pagination: ?page={int}&limit={int}')
        ->name("builders")
      )
      ->endpoint(ApiEndpoint::post('/', 'create')
        ->allow(AclRoles::USER)
        ->name("builder-create")
      )
      ->endpoint(ApiEndpoint::post('/{id}', 'update')
        ->allow(AclRoles::USER)
        ->name("builder-update")
      );
  }
}
