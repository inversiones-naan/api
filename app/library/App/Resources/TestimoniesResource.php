<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\Testimonies;
use App\Transformers\TestimoniesTransformer;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;

class TestimoniesResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Testimonies')
      ->model(Testimonies::class)
      ->expectsJsonData()
      ->transformer(TestimoniesTransformer::class)
      ->handler(CrudResourceController::class)
      ->itemKey('testimony')
      ->collectionKey('testimonies')
      ->deny(AclRoles::UNAUTHORIZED)
      ->handler(FundingController::class)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::create()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED)
      )
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED))
      ->endpoint(ApiEndpoint::remove()
        ->allow(AclRoles::MANAGER, AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::USER, AclRoles::AUTHORIZED)
      );
  }
}
