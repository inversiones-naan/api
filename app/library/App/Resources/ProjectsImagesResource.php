<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\ProjectsImages;
use App\Transformers\ProjectsImagesTransformer;
use App\Constants\AclRoles;
use PhalconRest\Mvc\Controllers\CrudResourceController;
use App\Controllers\UploadController;

class ProjectsImagesResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('ProjectsImages')
      ->model(ProjectsImages::class)
      ->expectsJsonData()
      ->transformer(ProjectsImagesTransformer::class)
      ->itemKey('projectImage')
      ->collectionKey('ProjectsImages')
      ->deny(AclRoles::UNAUTHORIZED)
      // ->handler(CrudResourceController::class)
      ->handler(UploadController::class)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::post('/', 'projectimages')
        ->description('Creates a new item using the posted data')
        ->name("projectimages")
      )
      // ->endpoint(ApiEndpoint::create())
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update())
      ->endpoint(ApiEndpoint::remove());
  }
}
