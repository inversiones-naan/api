<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\Funding;
use App\Transformers\FundingTransformer;
use App\Constants\AclRoles;
use App\Controllers\FundingController;
// use PhalconRest\Mvc\Controllers\CrudResourceController;

class FundingResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Funding')
      ->model(Funding::class)
      ->expectsJsonData()
      ->transformer(FundingTransformer::class)
      ->itemKey('fund')
      ->collectionKey('funding')
      ->deny(AclRoles::UNAUTHORIZED)
      ->handler(FundingController::class)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::create())
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update())
      ->endpoint(ApiEndpoint::remove())
      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns transactions using pagination: ?page={int}&limit={int}&userId={int}&projectId={int}&wordUser={string}&wordProject={string}')
        ->name("pages")
      );
  }
}
