<?php

namespace App\Resources;

use PhalconRest\Api\ApiEndpoint;
use PhalconRest\Api\ApiResource;
use App\Model\Instruments;
use App\Transformers\InstrumentsTransformer;
use App\Constants\AclRoles;
use App\Controllers\InstrumentsController;

class InstrumentsResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Instruments')
      ->model(Instruments::class)
      ->expectsJsonData()
      ->transformer(InstrumentsTransformer::class)
      ->itemKey('instrument')
      ->collectionKey('instruments')
      ->deny(AclRoles::UNAUTHORIZED)
      ->handler(InstrumentsController::class)

      ->endpoint(ApiEndpoint::all())
      ->endpoint(ApiEndpoint::create())
      ->endpoint(ApiEndpoint::find())
      ->endpoint(ApiEndpoint::update())
      ->endpoint(ApiEndpoint::remove())
      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns instruments using pagination: ?page={int}&limit={int}')
        ->name("instruments")
      );
  }
}
