<?php

namespace App\Resources;

use PhalconRest\Api\ApiResource;
use PhalconRest\Api\ApiEndpoint;
use App\Model\Users;
use App\Transformers\UsersTransformer;
use App\Controllers\UsersController;
use App\Constants\AclRoles;

class UsersResource extends ApiResource {

  public function initialize()
  {
    $this
      ->name('Users')
      ->model(Users::class)
      ->expectsJsonData()
      ->transformer(UsersTransformer::class)
      ->handler(UsersController::class)
      ->itemKey('user')
      ->collectionKey('users')
      ->deny(AclRoles::UNAUTHORIZED, AclRoles::USER)

      ->endpoint(ApiEndpoint::all()
        ->allow(AclRoles::USER)
        ->description('Returns all registered users')
      )
      ->endpoint(ApiEndpoint::find()
        ->allow(AclRoles::USER)
        ->name("findUser")
      )
      ->endpoint(ApiEndpoint::get('/me', 'me')
        ->allow(AclRoles::USER)
        ->description('Returns the currently logged in user')
        ->name("me") 
      )
      ->endpoint(ApiEndpoint::get('/pages', 'pages')
        ->allow(AclRoles::USER)
        ->description('Returns users using pagination')
        ->name("pages")
      )
      ->endpoint(ApiEndpoint::post('/authenticate', 'authenticate')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('Authenticates user credentials provided in the authorization header and returns an access token')
        ->exampleResponse([
          'token' => 'co126bbm40wqp41i3bo7pj1gfsvt9lp6',
          'expires' => 1451139067
        ])
      )
      ->endpoint(ApiEndpoint::post('/', 'create')
        ->allow(AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::UNAUTHORIZED)
        ->description('Creates a new item using the posted data')
        ->name("create")
      )
      ->endpoint(ApiEndpoint::put('/{id}', 'updateaccount')
        ->allow(AclRoles::ADMINISTRATOR)
        ->deny(AclRoles::UNAUTHORIZED)
        ->description('	Updates an existing item identified by {id}, using the posted data')
        ->name("updateaccount")
      )
      ->endpoint(ApiEndpoint::put('/me', 'meedit')
        ->allow(AclRoles::USER)
        ->description('Updating the currently logged in user')
        ->name("meedit") 
      )
      ->endpoint(ApiEndpoint::post('/sigin', 'sigin')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('Register user with email and password')
        ->name("sigin")
      )
      ->endpoint(ApiEndpoint::put('/activate-account', 'activateaccount')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('Activate user ?code={alphanumeric}')
        ->name("activateaccount")
      )
      ->endpoint(ApiEndpoint::put('/new-password', 'newpassword')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('New password using email')
        ->name("newpassword")
      )
      ->endpoint(ApiEndpoint::put('/activate-password', 'activatepassword')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('Activate password user ?code={alphanumeric}')
        ->name("activatepassword")
      )
      ->endpoint(ApiEndpoint::put('/new-activate', 'newactivate')
        ->allow(AclRoles::UNAUTHORIZED)
        ->deny(AclRoles::AUTHORIZED)
        ->description('New activate using email')
        ->name("newactivate")
      );
  }
}