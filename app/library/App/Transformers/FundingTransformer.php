<?php

namespace App\Transformers;

use App\Model\Funding;
// use PhalconRest\Transformers\Transformer;
use PhalconRest\Transformers\ModelTransformer;

class FundingTransformer extends ModelTransformer
{
  protected $modelClass = Funding::class;

  protected $availableIncludes = [
    'users'
  ];

  public function includeUsers($project)
  {
    return $this->collection($project->getUsers(), new UsersTransformer);
  }

  public function transform($fund)
  {
    return [
      'id' => $this->int($fund->id),
      'project' => $fund->getProjects(['columns'=>['id','name']]),
      'user' => $fund->getUsers(['columns'=>['id','firstName','lastName']]),
      'value' => $fund->value,
      'type' => $fund->type,
      'status' => $fund->status
    ];
  }
}
