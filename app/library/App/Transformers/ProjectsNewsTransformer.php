<?php

namespace App\Transformers;

use App\Model\ProjectsNews;
use PhalconRest\Transformers\ModelTransformer;

class ProjectsNewsTransformer extends ModelTransformer
{
  protected $modelClass = ProjectsNews::class;

  protected $availableIncludes = [
    'project',
    'creator'
  ];

  public function includeCreator($projectNew)
  {
    return $this->item($projectNew->getCreator(), new UsersTransformer);
  }

  public function includeProject($projectNew)
  {
    return $this->item($projectNew->getProjects(), new ProjectsTransformer);
  }
  
  public function transform($projectNew)
  {
    return [
      'id' => $this->int($projectNew->id),
      'projectId' => $this->int($projectNew->projectId),
      'creatorId' => $this->int($projectNew->creatorId),
      'text' => $projectNew->text,
      'updatedAt' => $projectNew->updatedAt
    ];
  }
}
