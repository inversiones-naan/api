<?php

namespace App\Transformers;

use App\Model\Testimonies;
use PhalconRest\Transformers\ModelTransformer;

class TestimoniesTransformer extends ModelTransformer
{
  protected $modelClass = Testimonies::class;

  public function transform($testimony)
  {
    return [
      'id' => $this->int($testimony->id),
      'name' => $testimony->name,
      'description' => $testimony->description,
      'url' => $testimony->url
    ];
  }
}
