<?php

namespace App\Transformers;

use App\Model\Instruments;
// use PhalconRest\Transformers\Transformer;
use PhalconRest\Transformers\ModelTransformer;

class InstrumentsTransformer extends ModelTransformer
{
  protected $modelClass = Instruments::class;

  protected $availableIncludes = [
    'projects'
  ];

  public function includeProjects($instrument)
  {
    return $this->collection($instrument->getProjects(), new ProjectsTransformer);
  }

  public function transform($instrument)
  {
    return [
      'id' => $this->int($instrument->id),
      'name' => $instrument->name
    ];
  }
}
