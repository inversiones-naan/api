<?php

namespace App\Transformers;

use App\Model\Builders;
// use PhalconRest\Transformers\Transformer;
use PhalconRest\Transformers\ModelTransformer;

class BuildersTransformer extends ModelTransformer
{
  protected $modelClass = Builders::class;

  protected $availableIncludes = [
    'projects'
  ];

  public function includeProjects($builder)
  {
    return $this->collection($builder->getProjects(), new ProjectsTransformer);
  }

  public function transform($builder)
  {
    // return [
    //   'id' => $this->int($builder->id),
    //   'name' => $builder->name,
    //   'description' => $builder->description,
    //   'urlImage' => $builder->urlImage,
    //   'url' => $builder->url
    // ];
    return $builder->toArray();
  }
}
