<?php

namespace App\Transformers;

use App\Model\UsersProjects;
// use PhalconRest\Transformers\Transformer;
use PhalconRest\Transformers\ModelTransformer;

class UsersProjectsTransformer extends ModelTransformer
{
  protected $modelClass = UsersProjects::class;

  protected $availableIncludes = [];

  // public function includeUsers($project)
  // {
  //   return $this->collection($project->getUsers(), new UsersTransformer);
  // }

  public function transform( $userProject)
  {
    return [
      'id' => $this->int($userProject->id),
      'user' => $userProject->getUsers()->username,
      'project' => $userProject->getProjects()->name,
      'status' => $userProject->status
    ];
  }
}
