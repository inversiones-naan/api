<?php

namespace App\Transformers;

use App\Model\Projects;
use PhalconRest\Transformers\ModelTransformer;

class ProjectsTransformer extends ModelTransformer
{
  /**
  * Transforms are automatically handled
  * based on your model when you extend ModelTransformer
  * and assign the modelClass property
  */
  protected $modelClass = Projects::class;

  protected $availableIncludes = [
    'users',
    'instrument',
    'builder',
    'images',
    'news'
  ];

  public function includeUsers($project)
  {
    return $this->collection($project->getUsers(), new UsersProjectsTransformer);
  }
  public function includeInstrument($project)
  {
    return $this->item($project->getInstrument(), new InstrumentsTransformer);
  }
  public function includeBuilder($project)
  {
    return $this->item($project->getBuilder(), new BuildersTransformer);
  }
  public function includeImages($project)
  {
    return $this->collection($project->getImages('status = 1'), new ProjectsImagesTransformer);
  }
  public function includeNews($project)
  {
    return $this->collection($project->getProjectsNews(), new ProjectsNewsTransformer);
  }
  /**
  * You can always transform manually by using
  * the following code (below):
  *
  public function transform(Photo $photo)
  {
    return [
      'id' => $this->int($photo->id),
      'title' => $photo->title,
      'albumId' => $this->int($photo->albumId)
    ];
  }
  */

  public function transform ($project)// (Projects $project)
  {
    return [
      'id' => $this->int($project->id),
      'name' => $project->name,
      'builderId'=> $this->int($project->builderId),
      'address' => $project->address,
      'fixedAnnualRate' => $project->fixedAnnualRate,
      'instrumentId'=> $this->int($project->instrumentId),
      'term' => $project->term,
      'funding' => $project->funding,
      'progressFunding' => $project->progressFunding,
      'possibilityOfPrepaid' => $project->possibilityOfPrepaid,
      'updatedAt' => $project->updatedAt,
      'createdAt' => $project->createdAt,
      'status' => $project->status,
      'minimumInvestment'=> $project->minimumInvestment,
      'description' => $project->description,
      'investment' => $project->investment,
      'googleLat'=> $project->googleLat,
      'googleLng'=> $project->googleLng,
      'countTransaction' => $project->getFunding()->count(),
      'countImages' => $project->getImages()->count(),
      'countNews' => $project->getNews()->count()
    ];
  }
}
