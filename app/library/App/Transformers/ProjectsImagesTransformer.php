<?php

namespace App\Transformers;

use App\Model\ProjectsImages;
// use PhalconRest\Transformers\Transformer;
use PhalconRest\Transformers\ModelTransformer;

class ProjectsImagesTransformer extends ModelTransformer
{
  protected $modelClass = ProjectsImages::class;

  protected $availableIncludes = [];

  // public function includeUsers($projectImage)
  // {
  //   return $this->collection($projectImage->getUsers(), new UsersTransformer);
  // }
  public function transform ($projectImage)
  {
    return [
      'id' => $this->int($projectImage->id),
      'project' => $projectImage->getProjects()->name,
      'title'  => $projectImage->title,
      'url' => $projectImage->url,
      'status' => $this->int($projectImage->status)
    ];
  }
}
