<?php

namespace App\Transformers;

use App\Model\Users;
use PhalconRest\Transformers\ModelTransformer;

class UsersTransformer extends ModelTransformer
{
    protected $modelClass = Users::class;

    protected function excludedProperties()
    {
        return ['password', 'code'];
    }
}