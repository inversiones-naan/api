<?php

namespace App\Constants;

class FundingTypes
{
    const INVEST = 0; // Inversión al proyecto
    const PAY = 1; // Pago al inversionista

    const ALL = [self::INVEST, self::PAY];
}