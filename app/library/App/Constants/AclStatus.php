<?php

namespace App\Constants;

class AclStatus
{
    const PENDING = 0;
    const ACTIVE = 1;
    const BANNED = 2;
    const INACTIVE = 3;
}