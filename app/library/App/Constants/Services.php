<?php

namespace App\Constants;

class Services extends \PhalconRest\Constants\Services
{
  const CONFIG = 'config';
  const VIEW = 'view';
  const API_SERVICE = 'api';
  const RANDOM_SERVICE = 'randomService';
  const MAIL_SERVICE = 'mail';
}
