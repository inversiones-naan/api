<?php

namespace App\Constants;

class ProjectStatus
{
    const INACTIVE = 0;
    const INVESTING = 1;
    const INVESTING_COMPLETED = 2;
    const GENERATING_RETURNS = 3;
    const FINISHED = 4;

    const ALL = [self::INACTIVE, self::INVESTING, self::INVESTING_COMPLETED, self::GENERATING_RETURNS, self::FINISHED];
}