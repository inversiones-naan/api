<?php

namespace App\Constants;

class UserProjectStatus
{
    const INACTIVE = 0; // Usuario inactivo en el proyecto
    const ACTIVE = 1; // Usuario activo en el proyecto
    const DELECTED = 2; // Usuario eliminado proyecto

    const ALL = [self::INACTIVE, self::ACTIVE, self::DELECTED];
}