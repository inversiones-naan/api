<?php

namespace App\Constants;

class FundingStatus
{
    const PENDING = 0;
    const ACTIVE = 1;

    const ALL = [self::PENDING, self::ACTIVE];
}