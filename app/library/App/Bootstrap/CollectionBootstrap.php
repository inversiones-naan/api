<?php

namespace App\Bootstrap;

use App\BootstrapInterface;
use App\Collections\ExportCollection;
use App\Resources\UsersResource;
use App\Resources\AlbumResource;
use App\Resources\PhotoResource;
use App\Resources\ProjectsResource;
use App\Resources\BuildersResource;
use App\Resources\InstrumentsResource;
use App\Resources\ProjectsImagesResource;
use App\Resources\UsersProjectsResource;
use App\Resources\FundingResource;
use App\Resources\TestimoniesResource;
use App\Resources\ProjectsNewsResource;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconRest\Api;

class CollectionBootstrap implements BootstrapInterface
{
  public function run(Api $api, DiInterface $di, Config $config)
  {
    $api
      ->collection(new ExportCollection('/export'))
      ->resource(new UsersResource('/users'))
      ->resource(new ProjectsResource('/projects'))
      ->resource(new BuildersResource('/builders'))
      ->resource(new InstrumentsResource('/instruments'))
      ->resource(new ProjectsImagesResource('/projects-images'))
      ->resource(new UsersProjectsResource('/users-projects'))
      ->resource(new FundingResource('/funding'))
      ->resource(new TestimoniesResource('/testimonies'))
      ->resource(new ProjectsNewsResource('/project-news'))
      // ->resource(new AlbumResource('/albums'))
      // ->resource(new PhotoResource('/photos'))
      ;
  }
}
