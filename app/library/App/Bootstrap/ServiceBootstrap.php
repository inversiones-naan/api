<?php

namespace App\Bootstrap;

use Phalcon\Config;
use PhalconRest\Api;
use Phalcon\DiInterface;
use App\BootstrapInterface;
use App\Constants\Services;
use App\Auth\UsernameAccountType;
use App\Auth\CodeAccountType;
use App\Fractal\CustomSerializer;
use Phalcon\Mvc\Url as UrlResolver;
// use Phalcon\Mvc\View\Simple as View;
use Phalcon\Mvc\View;
use App\Services\User as UserService;
use App\Services\Mail as MailService;
use App\Services\Random as RandomService;
use App\Auth\Manager as AuthManager;
use Phalcon\Events\Manager as EventsManager;
use League\Fractal\Manager as FractalManager;
use Phalcon\Mvc\Model\Manager as ModelsManager;
use PhalconApi\Auth\TokenParsers\JWTTokenParser;

class ServiceBootstrap implements BootstrapInterface
{
  public function run(Api $api, DiInterface $di, Config $config)
  {
    /**
     * @description Config - \Phalcon\Config
     */
    $di->setShared(Services::CONFIG, $config);

    /**
     * @description Phalcon - \Phalcon\Db\Adapter\Pdo\Mysql
     */
    $di->set(Services::DB, function () use ($config, $di) {

      $config = $config->get('database')->toArray();
      $adapter = $config['adapter'];
      unset($config['adapter']);
      $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

      $connection = new $class($config);

      // Assign the eventsManager to the db adapter instance
      $connection->setEventsManager($di->get(Services::EVENTS_MANAGER));

      return $connection;
    });

    /**
     * @description Phalcon - \Phalcon\Mvc\Url
     */
    $di->set(Services::URL, function () use ($config) {

      $url = new UrlResolver;
      $url->setBaseUri($config->get('application')->baseUri);
      return $url;
    });

    /**
     * @description Phalcon - \Phalcon\Mvc\View\Simple
     */
    $di->set(Services::VIEW, function () use ($config) {

      $class = new View;
      $class->setViewsDir($config->get('application')->viewsDir);
      $class->registerEngines(array(
        '.volt'=> function ($view, $di) {
          $volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
          // $volt->setOptions(array(
          //   'compiledPath'=> APPLICATION_PATH .'/compiled/',
          //   'stat'=> true,
          //   'compileAlways'=> true
          // ));
          return $volt;
        },
        '.phtml'=> 'Phalcon\Mvc\View\Engine\Php'
      ));
      return $class;
    });

    /**
     * @description Phalcon - EventsManager
     */
    $di->setShared(Services::EVENTS_MANAGER, function () use ($di, $config) {

      return new EventsManager;
    });

    /**
     * @description Phalcon - TokenParsers
     */
    $di->setShared(Services::TOKEN_PARSER, function () use ($di, $config) {

      return new JWTTokenParser($config->get('authentication')->secret, JWTTokenParser::ALGORITHM_HS256);
    });

    /**
     * @description Phalcon - AuthManager
     */
    $di->setShared(Services::AUTH_MANAGER, function () use ($di, $config) {

      $authManager = new AuthManager($config->get('authentication')->expirationTime);
      $authManager->registerAccountType(UsernameAccountType::NAME, new UsernameAccountType);
      $authManager->registerAccountType(CodeAccountType::NAME, new CodeAccountType);

      return $authManager;
    });

    /**
     * @description Phalcon - \Phalcon\Mvc\Model\Manager
     */
    $di->setShared(Services::MODELS_MANAGER, function () use ($di) {

      $modelsManager = new ModelsManager;
      return $modelsManager->setEventsManager($di->get(Services::EVENTS_MANAGER));
    });

    /**
     * @description PhalconRest - \League\Fractal\Manager
     */
    $di->setShared(Services::FRACTAL_MANAGER, function () {

      $fractal = new FractalManager;
      $fractal->setSerializer(new CustomSerializer);

      return $fractal;
    });

    /**
     * @description PhalconRest - \PhalconRest\User\Service
     */
    $di->setShared(Services::USER_SERVICE, new UserService);

    $di->set('modelsMetadata', function() { return new \Phalcon\Mvc\Model\MetaData\Memory(); });

    /**
     * @description PhalconRest - \PhalconRest\User\Service
     */
    $di->setShared(Services::MAIL_SERVICE, new MailService);

    /**
    * @description PhalconRest - \PhalconRest\User\Service
    */
    $di->setShared(Services::RANDOM_SERVICE, new RandomService);
  }
}
