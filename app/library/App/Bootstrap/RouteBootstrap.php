<?php

namespace App\Bootstrap;

use App\BootstrapInterface;
use App\Constants\Services;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconRest\Api;

class RouteBootstrap implements BootstrapInterface
{
  public function run(Api $api, DiInterface $di, Config $config)
  {
    $api->get('/', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);

      // return $view->render('general/index.volt');
      return $view->getRender('general','index');
    });

    $api->get('/proxy.html', function() use ($api, $config) {

      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);

      $view->setVar('client', $config->clientHostName);
      // return $view->render('general/proxy');
      return $view->getRender('general','proxy');
    });

    $api->get('/documentation.html', function() use ($api, $config) {

      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);

      $view->setVar('title', $config->application->title);
      $view->setVar('description', $config->application->description);
      $view->setVar('documentationPath', $config->hostName . '/export/documentation.json');
      // return $view->render('general/documentation.phtml');
      return $view->getRender('general','documentation');
    });

    $api->get('/activate/:params', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);

      return $view->getRender('general','index');
    });

    $api->get('/login', function() use ($api) { 
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/sigin', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/admin', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/admin/:params', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/inicio', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/activate-account', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/activate-password', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/proyectos', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/sign_up', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/inicio', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/mi-perfil', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/proyecto/:params', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/portafolio', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });
    $api->get('/portafolio/:params', function() use ($api) {
      /** @var \Phalcon\Mvc\View\Simple $view */
      $view = $api->di->get(Services::VIEW);
      return $view->getRender('general','index');
    });

    // $api->get('(/.*)*', function() use ($api) {
      
    //   /** @var \Phalcon\Mvc\View\Simple $view */
    //   $view = $api->di->get(Services::VIEW);

    //   return $view->render('general/index');
    // });
  }
}