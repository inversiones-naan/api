{% if confirmUrl is not defined %}
  {% set confirmUrl = 'confirmUrl' %}
{% endif %}
{% if year is not defined %}
  {% set year = date('Y', time()) %}
{% endif %}



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; width: 100%; height: 100%; margin: 0; padding: 0; background: #fafafe;">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Email Rendex</title>

		<style>
a[x-apple-data-detectors] {
  color: inherit !important;
  text-decoration: none !important;
  font-size: inherit !important;
  font-family: inherit !important;
  font-weight: inherit !important;
  line-height: inherit !important;
}

#outlook a {
  padding: 0;
}

.yshortcuts a {
  border-bottom: none !important;
}

.ExternalClass {
  width: 100%;
}

.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
  line-height: 100%;
}
</style>
		<style>
@media (max-width: 600px) {
  .body-table-spacing {
    padding-top: 0 !important;
    padding-bottom: 0 !important;
  }

  img {
    max-width: 100%;
    height: auto !important;
  }

  .container {
    width: 100% !important;
  }

  .container td {
    width: 100% !important;
    display: block;
    box-sizing: border-box;
  }

  .column-1.first,
.column-2.first,
.column-3.first,
.column-4.first,
.column-5.first,
.column-6.first,
.column-7.first,
.column-8.first,
.column-9.first,
.column-10.first,
.column-11.first,
.column-12.first {
    padding-left: 10px !important;
  }

  .column-1.last,
.column-2.last,
.column-3.last,
.column-4.last,
.column-5.last,
.column-6.last,
.column-7.last,
.column-8.last,
.column-9.last,
.column-10.last,
.column-11.last,
.column-12.last {
    padding-right: 10px !important;
  }

  .hero {
    height: auto !important;
    padding-top: 20px;
    padding-right: 20px;
    padding-bottom: 20px;
    padding-left: 20px;
  }

  .hero__inner {
    height: auto !important;
  }

  .hero__inner>table {
    width: 100% !important;
  }
}
</style>
	</head>
	<body style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; width: 100%; height: 100%; margin: 0; padding: 0; background: #fafafe; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;">
		<table id="preheader" width="0" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;">
			<tr>
				<td style="border-collapse: collapse; text-align: left; overflow: hidden; opacity: 0; visibility: hidden; mso-hide: all; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; color: #fff; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif; display: none;" align="left">Your preheader text goes here. This block should be hidden.</td>
			</tr>
		</table>
		<table id="body-table" width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0; padding: 0; background: #fafafe; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif; width: 100%; height: 100%; line-height: 100%;">
			<tr>
				<td class="body-table-spacing" align="left" valign="top" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; padding-bottom: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;">
					<table class="container" width="600" align="center" border="0" cellpadding="0" cellspacing="0" style="width: 600px; margin: 0 auto 0 auto; background: #fff; border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;">
						<tr>
							<td class="spacing-top spacing-bottom" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; padding-bottom: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
							</td>	
						</tr>
						<tr>
							<td style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
								<center>
									
								</center>
							</td>
						</tr>
						<tr>
							<td class="spacing-top" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
							</td>	
						</tr>
						<tr>
							<td class="column-12 first last" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; width: 600px; padding-left: 20px; padding-right: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
								<center>
									<h4 style="margin: 0; font-size: 21px; line-height: 29px;">Se ha creado el siguiente codigo...</h4>
								</center>
							</td>
                        </tr>
                        <tr>
							<td class="spacing-top" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
							</td>	
						</tr>
						<tr>
							<td class="column-12 first last" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; width: 600px; padding-left: 20px; padding-right: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
								<p>Bienvenido a Rendex, la herramienta que cambiará la forma de invertir tu dinero. Ahora es posible recuperar tu contraseña. Haz click en el siguiente link para activar completar el proceso.</p>
							</td>
						</tr>
						<tr>
							<td class="spacing-top" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left"/>
						</tr>
						<tr>
							<td class="column-12 first last" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; width: 600px; padding-left: 20px; padding-right: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left">
								<center>
									<table class="btn btn--chronos btn--rounded" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; mso-table-lspace: 0pt; mso-table-rspace: 0pt; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;">
										<tr>
											<td style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: center; padding-top: 8px; padding-right: 35px; padding-bottom: 8px; padding-left: 35px; background-color: #d6199b; border-radius: 50px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="center" bgcolor="#d6199b">
												<a href="{{confirmUrl}}" style="color: #fff; text-decoration: none;">Recuperar contraseña</a>
											</td>
										</tr>
									</table>
								</center>
							</td>
						</tr>
						<tr>
							<td class="spacing-top spacing-bottom" style="font-size: 16px; line-height: 23px; border-collapse: collapse; text-align: left; padding-top: 20px; padding-bottom: 20px; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif;" align="left"/>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>